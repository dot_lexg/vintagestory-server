# frozen_string_literal: true

require 'pathname'
require 'rake/clean'

#### Build configuration
dist_version   = '1.19.3'
dist_url       = "https://cdn.vintagestory.at/gamefiles/stable/vs_server_linux-x64_#{dist_version}.tar.gz"
image_tag      = "localhost/vintagestory-server:#{dist_version}"

#### Run configuration
container_name = 'vintagestory-server'
server_bind    = '0.0.0.0:42420'

#### Path setup
project_path       = Pathname(__dir__).relative_path_from(Dir.pwd)
source_path        = project_path / 'src'
build_path         = project_path / 'build'
explode_path       = project_path / 'image'
data_path          = project_path / 'data'

# /build is docker's build context, and /src is copied to /build/src so docker can access it
dist_path          = build_path / File.basename(dist_url)
dockerfile_path    = build_path / 'src/Dockerfile'

build_path.mkdir unless build_path.directory?
data_path.mkdir unless data_path.directory?
CLEAN << build_path << explode_path

systemd_service_path = Pathname('~/.config/systemd/user/vintagestory-server.service').expand_path

#### Helpers
def sh_capture(*command, **)
  command_str = "#{sh_show_command(command)} > ..."

  Rake.rake_output_message command_str
  result = IO.popen(command, **, &:read)
  status = Process.last_status
  raise "Command failed with status (#{status.exitstatus}): [#{command_str}]" unless status.success?

  result.chomp
end

### Tasks
task default: [:build]

desc "Build the docker image #{image_tag.inspect}"
task build: [dist_path] do
  sh 'cp', '-rv', source_path.to_s, build_path.to_s
  sh 'docker', 'buildx', 'build',
     "--build-arg=DIST_BASENAME=#{dist_path.basename}",
     "--build-arg=RUNTIME_UID=#{Process.uid}",
     '-t', image_tag.to_s,
     '-f', dockerfile_path.to_s,
     build_path.to_s
end

desc "Build and copy filesystem in #{image_tag} to ./#{explode_path}"
task explode: [:build] do
  sh 'rm', '-rf', explode_path.to_s
  container_id = sh_capture 'docker', 'container', 'create', image_tag
  sh 'docker', 'container', 'cp', "#{container_id}:/", "./#{explode_path}"
ensure
  sh 'docker', 'container', 'rm', container_id, out: '/dev/null' if container_id
end

desc "Build and run #{image_tag} as a container #{container_name.inspect} interactively"
task run: [:build] do
  sh 'docker', 'run', '--rm',
     "--user=#{Process.uid}",
     '--name', container_name,
     '-p', "#{server_bind}:42420",
     '--mount', "type=bind,source=#{data_path.realpath},target=/vintagestory-server/data",
     image_tag
end

desc 'Send a command to the running server'
task :command, [:line] do |_t, args|
  sh 'docker', 'exec', container_name, '/vintagestory-server/bin/command', args.line
end

namespace :systemd do
  desc "Add a systemd service at #{systemd_service_path}"
  task install: [:uninstall, systemd_service_path]

  file systemd_service_path do
    puts "Writing to #{systemd_service_path}"
    File.write(systemd_service_path, <<~DESCRIPTOR)
      [Unit]
      Description=Vintage Story dedicated server

      [Service]
      Type=exec
      WorkingDirectory=#{project_path.realpath}
      ExecStart=#{RbConfig::CONFIG['bindir']}/rake run
      ExecStop=#{RbConfig::CONFIG['bindir']}/rake command[stop]
      SuccessExitStatus=0 1

      [Install]
      WantedBy=default.target
    DESCRIPTOR
    sh 'systemctl', '--user', 'daemon-reload'
  end

  desc "Remove the systemd service at #{systemd_service_path}"
  task :uninstall do
    sh 'rm', '-f', systemd_service_path.to_s
    sh 'systemctl', '--user', 'daemon-reload'
  end

  [:enable, :disable, :status, :start, :stop, :restart].each do |verb|
    desc "#{verb.capitalize} vintagestory-server.service with systemd"
    task verb => [systemd_service_path] do
      sh 'systemctl', '--user', verb.to_s, 'vintagestory-server.service'
    end
  end
end

file dist_path do
  sh 'curl', '-#flo', dist_path.to_s, dist_url
end
